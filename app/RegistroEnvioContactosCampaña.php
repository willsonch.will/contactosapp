<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegistroEnvioContactosCampaña extends Model
{
    protected $guarded = [];
    protected $fillable =[
    'campaña_id','contacto_id','estado'];
    
    public function campaña()
	{
     return $this->belongsTo('App\Campañas','campaña_id','id');
    }
    public function contactoInteres()
	{
     return $this->belongsTo('App\ContactoInteres','contacto_id','id');
    }
}
