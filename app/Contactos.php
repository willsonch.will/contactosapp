<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contactos extends Model
{
    protected $fillable =[
        'nombre','celular','correo','telefono','direccion','fecha_nacimiento','sexo','vendedor_id'];

    public function vendedor()
        {
         return $this->belongsTo('App\Vendedores','vendedor_id','id');
        }
    public function contacto_interes()
        {
         return $this->belongsToMany('App\Interes', 'contacto_interes', 'contacto_id', 'interes_id');
        }
        public function detalle_interes()
    {
     return $this->hasMany('App\ContactoInteres','contacto_id','id');
    }
}
