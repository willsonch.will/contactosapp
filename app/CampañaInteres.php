<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampañaInteres extends Model
{
    protected $guarded = [];
    protected $fillable =[
    'campaña_id','interes_id'];

    public function interes()
	{
     return $this->belongsTo('App\Interes','interes_id','id');
    }
}
