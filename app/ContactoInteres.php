<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactoInteres extends Model
{
    protected $guarded = [];
    protected $fillable =[
    'contacto_id','interes_id'];
    
    public function contacto()
	{
     return $this->belongsTo('App\Contactos','contacto_id','id');
    }
    public function interes()
	{
     return $this->belongsTo('App\Interes','interes_id','id');
    }
}
