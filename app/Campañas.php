<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campañas extends Model
{
    protected $guarded = [];
    protected $fillable =[
    'vendedor_id','titulo','descripcion','foto','estado'];
    
    public function vendedor()
	{
     return $this->belongsTo('App\Vendedores','vendedor_id','id');
    }
    public function detalle_campaña_interes()
	{
     return $this->belongsToMany('App\Interes', 'campaña_interes', 'campaña_id', 'interes_id');
    }
    public function campaña_interes()
    {
     return $this->hasMany('App\CampañaInteres','campaña_id','id');
    }
    public function registro_envio_campaña_interes()
	{
     return $this->belongsToMany('App\ContactoInteres', 'registro_envio_contactos_campañas', 'campaña_id', 'contacto_id');
    }
}
