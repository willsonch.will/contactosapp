<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendedores extends Model
{
    protected $guarded = [];
    protected $fillable =[
    'nombre','celular','correo','fecha_nacimiento','sexo'];
}
