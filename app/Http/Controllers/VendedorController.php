<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Vendedores;

class VendedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendedor=Vendedores::all();
        $pageConfigs = [
            'theme' => 'dark',
            'navbarColor' => 'bg-primary',
            'navbarType' => 'static',
            'footerType' => 'sticky',
            'bodyClass' => 'testClass'
        ];
        return view('/vendedor/index', [
            'pageConfigs' => $pageConfigs,
            "vendedores" => $vendedor
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageConfigs = [
            'theme' => 'dark',
            'navbarColor' => 'bg-primary',
            'navbarType' => 'static',
            'footerType' => 'sticky',
            'bodyClass' => 'testClass'
        ];
        return view('/vendedor/form', [
            'pageConfigs' => $pageConfigs
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $vendedor=new Vendedores;
        $vendedor->nombre=$request->get('nombre');
        $vendedor->celular=$request->get('celular');
        $vendedor->correo=$request->get('correo');
        $vendedor->fecha_nacimiento=$request->get('fecha_nacimiento');
        $vendedor->sexo=$request->get('sexo');

        $vendedor->save();

        return Redirect::to('vendedor');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
