<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Contactos;
use App\Vendedores;
use App\Interes;

class ContactoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contactos=Contactos::all();
        $pageConfigs = [
            'theme' => 'dark',
            'navbarColor' => 'bg-primary',
            'navbarType' => 'static',
            'footerType' => 'sticky',
            'bodyClass' => 'testClass'
        ];
        return view('/contacto/index', [
            'pageConfigs' => $pageConfigs,
            'contactos' => $contactos
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vendedores = Vendedores::all();
        $interes = Interes::all();
        $pageConfigs = [
            'theme' => 'dark',
            'navbarColor' => 'bg-primary',
            'navbarType' => 'static',
            'footerType' => 'sticky',
            'bodyClass' => 'testClass'
        ];
        return view('/contacto/form', [
            'pageConfigs' => $pageConfigs,
            'vendedores' => $vendedores,
            'interes' => $interes
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contacto=new Contactos;
        $contacto->nombre=$request->get('nombre');
        $contacto->celular=$request->get('celular');
        $contacto->correo=$request->get('correo');
        $contacto->telefono=$request->get('telefono');
        $contacto->direccion=$request->get('direccion');
        $contacto->fecha_nacimiento=$request->get('fecha_nacimiento');
        $contacto->sexo=$request->get('sexo');
        $contacto->vendedor_id=$request->get('vendedor_id');

        $contacto->save();
        $interes=[];

        foreach($request->get('interes_id') as $i){
            $interes[]=Interes::find($i) ? $i : Interes::create(['descripcion'=>$i])->id;
        }
        $contacto->contacto_interes()->sync($interes);

        return Redirect::to('contacto');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
