<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Vendedores;
use App\Campañas;
use App\Contactos;
use App\Interes;
use App\CampañaInteres;
use App\ContactoInteres;
use App\RegistroEnvioContactosCampaña;
use DB;
use Carbon\Carbon;
use Mail;
use Twilio\Rest\Client;

class CampañaController extends Controller
{
    public function index()
    {
        $campaña = Campañas::with('vendedor')->orderBy('id','desc')->get();
        $pageConfigs = [
            'theme' => 'dark',
            'navbarColor' => 'bg-primary',
            'navbarType' => 'static',
            'footerType' => 'sticky',
            'bodyClass' => 'testClass'
        ];
      return view('/campaña/index', [
          'pageConfigs' => $pageConfigs,
          'campaña' => $campaña
      ]);
    }
    public function create()
    {
        $vendedores = Vendedores::all();
        $pageConfigs = [
            'theme' => 'dark',
            'navbarColor' => 'bg-primary',
            'navbarType' => 'static',
            'footerType' => 'sticky',
            'bodyClass' => 'testClass'
        ];
      return view('/campaña/create', [
          'pageConfigs' => $pageConfigs,
          'vendedores' => $vendedores
      ]);
    }
    public function store(Request $request)
    {
        $campaña=new Campañas; 
        $campaña->vendedor_id=$request->get('vendedor_id');
        $campaña->titulo=$request->get('titulo'); 
        $campaña->descripcion=$request->get('descripcion');
        if ($request->hasFile('foto')){
         $file=$request->file('foto');
         $file->move(public_path().'/images/campaña/',$file->getClientOriginalName());
            $campaña->foto=$file->getClientOriginalName();
        }
        $campaña->save();
        //return response()->json($campaña,201);
        return Redirect::to('registro-campaña');
    }
    public function campaña_interes($id)
    {
        $campaña=Campañas::findOrFail($id);
        $contactos=Contactos::select('id')->where('vendedor_id', $campaña->vendedor_id)->get();
        $contacto_interes = ContactoInteres::select(DB::raw('count(contacto_id) as cantidad'),'interes_id')
        ->with('interes')
        ->whereIn('contacto_id',$contactos)
        ->groupBy('interes_id')->get();
        $pageConfigs = [
            'theme' => 'dark',
            'navbarColor' => 'bg-primary',
            'navbarType' => 'static',
            'footerType' => 'sticky',
            'bodyClass' => 'testClass'
        ];
      return view('/campaña/compaña_interes', [
          'pageConfigs' => $pageConfigs,
          'campaña' => $campaña,
          'contacto_interes' => $contacto_interes
      ]);
    }
    public function registrar_campaña_interes(Request $request, $id)
    {
        $campaña=Campañas::findOrFail($id);
        $campaña->detalle_campaña_interes()->sync($request->get('interes_id'));
        return Redirect::to('registro-campaña');
    }
    public function registro_envio_campaña_contactos($id)
    {
        try{
            DB::beginTransaction();
                $campaña=Campañas::findOrFail($id);
            if (CampañaInteres::where('campaña_id',$id)->exists() && $campaña->estado=="BORRADOR") {
                
                $contactos=Contactos::select('id')->where('vendedor_id', $campaña->vendedor_id)->get();
                $campaña_interes=CampañaInteres::select('interes_id')->where('campaña_id', $id)->get();
                $contacto_interes = ContactoInteres::select('id')->with('interes')->whereIn('contacto_id',$contactos)->whereIn('interes_id',$campaña_interes)->get();
                $campaña->registro_envio_campaña_interes()->sync($contacto_interes);
                $campaña->estado='ENVIADO';
                $campaña->update();

                $fecha_envio = Carbon::now()->toDateString();
                $registro_envio=RegistroEnvioContactosCampaña::where('campaña_id', $id)->get();
                for ($i=0; $i < count($registro_envio); $i++) {
                    $registro=RegistroEnvioContactosCampaña::findOrFail($registro_envio[$i]->id);
                    //ENVIO PARA EMAIL
                    $data = [
                        'titulo' => $campaña->titulo,
                        'descripcion' => $campaña->descripcion,
                        'contacto' => $registro->contactoInteres->contacto->nombre,
                        'fecha' => $fecha_envio
                    ];
                    $vendedor = "Sr : ".$campaña->vendedor->nombre;
                    $subject = "Nuevo Curso : ".$campaña->titulo;
                    $for = $registro->contactoInteres->contacto->correo;
                    Mail::send('campaña.email',$data, function($msj) use($subject,$for,$vendedor){
                        $msj->from("juan.perez.app.contactos@gmail.com",$vendedor." (AppContactos)");
                        $msj->subject($subject);
                        $msj->to($for);
                    });
                    //ENVIO PARA WHATSAPP
                    $sid    = "AC47f616e44bd5433562bf73621eeb0cd9"; 
                    $token  = "541016322d68bbd4c8f2e87bbc262997"; 
                    $twilio = new Client($sid, $token); 
                    $message = $twilio->messages 
                    ->create("whatsapp:+591".$registro->contactoInteres->contacto->celular, // to 
                            array( 
                                "from" => "whatsapp:+14155238886",       
                                "body" => "CURSO *".$campaña->titulo."* Hola ".$registro->contactoInteres->contacto->nombre.", ".$campaña->descripcion,
                                'mediaUrl' => 'https://bit.ly/3phpLMz'
                            ) 
                    ); 
                }
            }
            DB::commit();
        }catch (Exception $e){
            DB::rollBack();
        } 
        return Redirect::to('registro-campaña');
        
    }
    public function registro_campaña_contactos($id)
    {
        $campaña=Campañas::findOrFail($id);
        $registro_contactos=RegistroEnvioContactosCampaña::where('campaña_id', $id)->get();
        $pageConfigs = [
            'theme' => 'dark',
            'navbarColor' => 'bg-primary',
            'navbarType' => 'static',
            'footerType' => 'sticky',
            'bodyClass' => 'testClass'
        ];
        return view('/campaña/compaña_registro_contactos', [
            'pageConfigs' => $pageConfigs,
            'campaña' => $campaña,
            'registro_contactos' => $registro_contactos
        ]);
    }
    public function cambiar_estado_campaña_interesados($id)
    {
        $registro_contactos=RegistroEnvioContactosCampaña::findOrFail($id);
        $registro_contactos->estado='INTERESADO';
        $registro_contactos->update();
        return redirect()->back();
    }
    public function cambiar_estado_campaña_nointerezados($id)
    {
        $registro_contactos=RegistroEnvioContactosCampaña::findOrFail($id);
        $registro_contactos->estado='NO INTERESADO';
        $registro_contactos->update();
        return redirect()->back();
    }
    public function cambiar_estado_campaña_convertido($id)
    {
        $registro_contactos=RegistroEnvioContactosCampaña::findOrFail($id);
        $registro_contactos->estado='CONVERTIDO';
        $registro_contactos->update();
        return redirect()->back();
    }
}
