<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampañasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campañas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('vendedor_id')->unsigned();
            $table->string('titulo',255);
            $table->string('descripcion',1000);
            $table->string('foto',1000)->nullable();
            $table->string('estado',50)->default('BORRADOR');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campañas');
    }
}
