<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contactos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('vendedor_id')->unsigned();
            $table->string('nombre');
            $table->string('celular',8);
            $table->string('correo');
            $table->string('telefono',8)->nullable();
            $table->string('direccion',255)->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->string('sexo',1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contactos');
    }
}
