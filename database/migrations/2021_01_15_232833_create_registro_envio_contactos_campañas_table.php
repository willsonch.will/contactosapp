<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistroEnvioContactosCampañasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro_envio_contactos_campañas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('campaña_id')->unsigned();
            $table->integer('contacto_id')->unsigned();
            $table->string('estado',50)->default('CONTACTADO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registro_envio_contactos_campañas');
    }
}
