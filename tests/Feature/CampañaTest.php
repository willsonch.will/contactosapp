<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CampañaTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    /** @test */
    public function store()
    {
        $response = $this->post('registro-campaña-create',[
            'vendedor_id' => '1',
            'titulo' => 'Curso',
            'descripcion' => 'Descripcion Curso'
        ]);

        /*$response->assertJsonStructure(['vendedor_id','titulo','descripcion'])
                 ->assertJson(['vendedor_id' => '1'])
                 ->assertStatus(201);*/
        
        $response->assertStatus(302);

        $this->assertDatabaseHas('campañas',[
            'vendedor_id' => '1',
            'titulo' => 'Curso',
            'descripcion' => 'Descripcion Curso',
            'foto' => null,
            'estado' => 'BORRADOR'
        ]);
    }
 
}
