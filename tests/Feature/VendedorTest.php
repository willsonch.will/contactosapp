<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class VendedorTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function store()
    {
        $response = $this->post('vendedor-form',[
            'nombre' => 'Sergio Cortez',
            'celular' => '87645364',
            'correo' => 'sergio@gmail.com',
            'fecha_nacimiento' => null,
            'sexo' => 'm'
        ]);

        //$response->assertJsonStructure(['nombre','celular','correo','fecha_nacimiento','sexo'])
        //         ->assertStatus(201);

        $this->assertDatabaseHas('vendedores',[
            'nombre' => 'Sergio Cortez',
            'celular' => '87645364',
            'correo' => 'sergio@gmail.com',
            'fecha_nacimiento' => null,
            'sexo' => 'm'
        ]);
    }
    
}