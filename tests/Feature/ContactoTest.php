<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContactoTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->get('/contacto');

        $response->assertStatus(200);
    }

    public function testCreate()
    {
        $response = $this->get('/contacto-form');

        $response->assertStatus(200);
    }

    public function testSore(){

            $response = $this->post('contacto-form',[
                "nombre" => "juan",
                "celular" => "78457845",
                "telefono" => "64-59874",
                "correo" => "es@gmail.com",
                "direccion" => "dir",
                "fecha_nacimiento" => null,
                "sexo" => "M",
                "vendedor_id" => "1",
                "interes_id" => [  
                    0 => "1",
                    1 => "2"
                ]
            ]);

            $response->assertStatus(302);
    
            $this->assertDatabaseHas('contactos',[
                            "nombre" => "juan",
                            "celular" => "78457845",
                            "telefono" => "64-59874",
                            "correo" => "es@gmail.com",
                            "direccion" => "dir",
                            "fecha_nacimiento" => null,
                            "sexo" => "M",
                            "vendedor_id" => "1"
                    ]);
    }
}
