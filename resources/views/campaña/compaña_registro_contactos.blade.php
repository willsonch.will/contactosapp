@extends('layouts/contentLayoutMaster')
@section('title', 'Campaña - Registro de Contactos')
@section('vendor-style')
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection
@section('page-style')
@endsection
@section('content')
<section id="basic-datatable">
    <div class="row">
         <div class="col-md-3">
            <div class="card">
                <div class="card-header">
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <h4>Titulo</h4>
                        <p>{{$campaña->titulo}}</p>
                        <div class="mt-1">
                            <h4 class="mb-0">Descripcion:</h4>
                            <p>{{$campaña->descripcion}}</p>
                        </div>
                        <div class="mt-1">
                            <h4 class="mb-0">Interes:</h4>
                            @foreach($campaña->campaña_interes as $campinteres)
                            <p class="mb-0">{{$campinteres->interes->descripcion}}</p>
                            @endforeach
                        </div>
                        <div class="mt-1">
                            <h4 class="mb-0">Responsable (Vendedor):</h4>
                            <p>{{$campaña->vendedor->nombre}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Listado de ofertas de los contactos</h4>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>CONTACTO</th>
                                        <th>CELULAR</th>
                                        <th>CORREO</th>
                                        <th>TELEFONO</th>
                                        <th>INTERES</th>
                                        <th>ESTADO</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($registro_contactos as $rc)
                                    <tr>
                                        <td>{{ $rc->id }}</td>
                                        <td>{{ $rc->contactoInteres->contacto->nombre }}</td>
                                        <td>{{ $rc->contactoInteres->contacto->celular }}</td>
                                        <td>{{ $rc->contactoInteres->contacto->correo }}</td>
                                        <td>{{ $rc->contactoInteres->contacto->telefono }}</td>
                                        <td>{{ $rc->contactoInteres->interes->descripcion }}</td>
                                        <td>{{ $rc->estado }}</td>
                                        <th>
                                            
                                            <div class="action-btns">
                                                <div class="btn-dropdown ">
                                                    <div class="btn-group dropdown actions-dropodown">
                                                        <button type="button" class="btn btn-white px-2 py-75 dropdown-toggle waves-effect waves-light"
                                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Cambiar
                                                        </button>
                                                        <div class="dropdown-menu" style="background-color: #10163a;">
                                                       
                                                        <a class="dropdown-item" href="{{URL::action('CampañaController@cambiar_estado_campaña_interesados',$rc->id)}}">
                                                        <i class="feather icon-trash-2"></i> INTERESADO</a>
                                                        
                                                        <a class="dropdown-item" href="{{URL::action('CampañaController@cambiar_estado_campaña_nointerezados',$rc->id)}}"><i class="feather icon-clipboard"></i> NO INTERESADO</a>
                                                        
                                                        <a class="dropdown-item" href="{{URL::action('CampañaController@cambiar_estado_campaña_convertido',$rc->id)}}"><i class="feather icon-printer"></i> CONVERTIDO</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                        </th>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>CONTACTO</th>
                                        <th>CELULAR</th>
                                        <th>CORREO</th>
                                        <th>TELEFONO</th>
                                        <th>INTERES</th>
                                        <th>ESTADO</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>


@endsection
@section('vendor-script')
        <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
@endsection
@section('page-script')
        <script src="{{ asset(mix('js/scripts/datatables/datatable.js')) }}"></script>
@endsection

