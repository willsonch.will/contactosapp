@extends('layouts/contentLayoutMaster')
@section('title', 'Campañas')
@section('vendor-style')
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection
@section('page-style')
@endsection
@section('content')
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Listado de Campañas</h4>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                        <a href="{{URL::action('CampañaController@create')}}">
                            <button id="addRow" class="btn btn-primary"><i class="feather icon-plus"></i>&nbsp; Registrar Nueva Campaña </button>
                        </a>
                        
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>VENDEDOR</th>
                                        <th>TITULO</th>
                                        <th>DESCRIPCION</th>
                                        <th>FOTOGRAFIA</th>
                                        <th>INTERES</th>
                                        <th>ESTADO</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($campaña as $camp)
                                    <tr>
                                        <td>{{ $camp->id }}</td>
                                        <td>{{ $camp->vendedor->nombre }}</td>
                                        <td>{{ $camp->titulo }}</td>
                                        <td>{{ $camp->descripcion }}</td>
                                        <td>
                                            @if($camp->foto=="")
                                            @else
                                            <img width='100px' height='100px' src="{{asset('images/campaña/').'/'.$camp->foto}}"> 
                                            @endif
                                        </td>
                                        <td>
                                            @foreach($camp->campaña_interes as $campinteres)
                                            <p><span class="badge badge-pill badge-light-primary mr-1">{{$campinteres->interes->descripcion}}</span></p>
                                            @endforeach
                                        </td>
                                        <!--<th>{{$camp->campaña_interes}}</th>-->
                                        <td>{{ $camp->estado }}</td>
                                        <th>
                                            <div class="action-btns">
                                                <div class="btn-dropdown ">
                                                <div class="btn-group dropdown actions-dropodown">
                                                    <button type="button" class="btn btn-white px-2 py-75 dropdown-toggle waves-effect waves-light"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Accion
                                                    </button>
                                                    <div class="dropdown-menu" style="background-color: #10163a;">
                                                    @if($camp->estado=='BORRADOR')
                                                    <a class="dropdown-item" href="{{URL::action('CampañaController@campaña_interes',$camp->id)}}"><i class="feather icon-trash-2"></i> Registrar Interes de Campaña</a>
                                                    @endif
                                                    @if($camp->estado!='ENVIADO')
                                                    <a class="dropdown-item" href="{{URL::action('CampañaController@registro_envio_campaña_contactos',$camp->id)}}"><i class="feather icon-clipboard"></i> Enviar Campaña a Contactos</a>
                                                    @endif
                                                    @if($camp->estado=='ENVIADO')
                                                    <a class="dropdown-item" href="{{URL::action('CampañaController@registro_campaña_contactos',$camp->id)}}"><i class="feather icon-printer"></i> Control de Campaña</a>
                                                    </div>
                                                    @endif
                                                </div>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>VENDEDOR</th>
                                        <th>TITULO</th>
                                        <th>DESCRIPCION</th>
                                        <th>FOTOGRAFIA</th>
                                        <th>INTERES</th>
                                        <th>ESTADO</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection
@section('vendor-script')
        <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
@endsection
@section('page-script')
        <script src="{{ asset(mix('js/scripts/datatables/datatable.js')) }}"></script>
@endsection

