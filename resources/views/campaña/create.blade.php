
@extends('layouts/contentLayoutMaster')

@section('title', 'Campañas')

@section('page-style')
            {{-- Page Css files --}}
            <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/validation/form-validation.css')) }}">
            <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
             <link rel="stylesheet" href="{{ asset(mix('css/pages/users.css')) }}">
@endsection
@section('content')
<!-- Simple Validation start -->
<section class="simple-validation">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Formulario de registro de campaña</h4>
        </div>
        <div class="card-content">
          <div class="card-body">
            <form method="POST" class="form-horizontal" novalidate action="{{ route('campaña.create') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="controls">
                                <select name="vendedor_id" id="idselect" class="select2 form-control" >
                                    @foreach($vendedores as $v)
                                        <option value="{{$v->id}}">{{$v->nombre}}</option>
                                    @endforeach
                                </select>                       
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="controls">
                            <input type="text" name="titulo" id="idtitulo" class="form-control" placeholder="Titulo Campaña" required
                                data-validation-required-message="Titulo Campaña es requerido">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="controls">
                                <textarea class="form-control" name="descripcion" id="basicTextarea" rows="3" placeholder="Descripcion" required data-validation-required-message="Descripcion Campaña es requerido"></textarea>                        
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="controls">
                                 <input name="foto" class="form-control"  type="file" >                        
                            </div>
                        </div>
                    </div>      
                </div>
                <button type="submit" class="btn btn-primary">Registrar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Vista Previa</h4>
        </div>
        <div class="card-content">
          <div class="card-body">
                <div class="d-flex justify-content-start align-items-center mb-1">
                    <div class="avatar mr-1">
                        <img src="{{ asset('images/profile/user-uploads/page-06.jpg') }}" alt="avtar img holder" height="45" width="45">
                    </div>
                    <div class="user-page-info">
                        <p id="idtextvendedor" class="mb-0"></p>
                        <span id="idtextfecha" class="font-small-2"></span>
                    </div>
                    <div class="ml-auto user-like text-danger"><i class="fa fa-heart"></i></div>
                    </div>
                    <h6 id="idtexttitulo" class="mb-1"></h6>
                    <p id="idtextdescripcion"></p>
                    <img class="img-fluid card-img-top rounded-sm" src="{{ asset('images/profile/post-media/3.jpg') }}" alt="avtar img holder">
                </div>  
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Input Validation end -->
@endsection

@section('vendor-script')
        <!-- vendor files -->
        <script src="{{ asset(mix('vendors/js/forms/validation/jqBootstrapValidation.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection
@section('page-script')
        <!-- Page js files -->
        <script src="{{ asset(mix('js/scripts/forms/validation/form-validation.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/pages/user-profile.js')) }}"></script>
        <script>
        $(document).ready(function () {
            horaA = new Date();
            var objetivof = document.getElementById('idtextfecha');
            objetivof.innerHTML = horaA

            vendedor=$("#idselect option:selected").text();
            var objetivo = document.getElementById('idtextvendedor');
            objetivo.innerHTML = vendedor
        });
        $("#idtitulo").keyup(titulo);
        function titulo(){
            titulo=$("#idtitulo").val();
            var objetivo = document.getElementById('idtexttitulo');
            objetivo.innerHTML = titulo
        }
        $("#basicTextarea").keyup(descripcion);
        function descripcion(){
            descripcion=$("#basicTextarea").val();
            var objetivo = document.getElementById('idtextdescripcion');
            objetivo.innerHTML = descripcion
        }
        $("#idselect").change(mostrarDatos);
        function mostrarDatos(){
            vendedor=$("#idselect option:selected").text();
            var objetivo = document.getElementById('idtextvendedor');
            objetivo.innerHTML = vendedor
        }
        </script>
        <script>
            $(".select2").select2({
                tags: true,
            })
        </script>
@endsection
