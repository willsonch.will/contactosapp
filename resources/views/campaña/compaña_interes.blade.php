@extends('layouts/contentLayoutMaster')
@section('title', 'App Calender')
@section('vendor-style')
@endsection
@section('page-style')
            <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/validation/form-validation.css')) }}">
            <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
             <link rel="stylesheet" href="{{ asset(mix('css/pages/users.css')) }}">
@endsection
@section('content')

<section class="simple-validation">
  <div class="row">
    <div class="col-md-6">
        <div class="card">
          <div class="card-header">
              <h4 class="card-title">Interes</h4>
          </div>
          <div class="card-content">
              <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>CANTIDAD DE CONTACTOS INTERESADOS</th>
                                    <th>INTERES</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($contacto_interes as $cont)
                                    <tr>
                                        <td>{{$cont->cantidad}}</td>
                                        <td>{{$cont->interes->descripcion}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <form method="POST" class="form-horizontal" novalidate action="{{ route('registro.campaña.interes',$campaña->id) }}">
	                {{ csrf_field() }} {{ method_field('POST') }}
                    <div class="form-group">
                            <div class="text-bold-600 font-medium-2">
                                Seleccion de Interes
                            </div>
                            <p>Seleccione los intereces de campaña para realizar las ofertas a los contactos o interesados</p>
                            <div class="controls">
                                <select required class="select2 form-control" name="interes_id[]" multiple="multiple" data-validation-required-message="Seleccione por lo menos un interes">
                                   @foreach($contacto_interes as $conti)
                                       <option {{collect(old('contacto_interes', $campaña->campaña_interes->pluck('interes_id')))->contains($conti->interes->id) ? 'selected' : ''}} value="{{$conti->interes->id}}">{{$conti->interes->descripcion}}</option>
                                    @endforeach
                                   
                                    <!--@foreach($contacto_interes as $conti)
                                        <option value="{{$conti->interes->id}}">{{$conti->interes->descripcion}}</option>
                                    @endforeach-->
                                </select>
                            </div>
                    </div>
                        <button type="submit" class="btn btn-primary">Registrar Interes de Campaña</button>
                    </form>
              </div>
          </div>
      </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
            <h4 class="card-title">Vista Previa</h4>
            </div>
            <div class="card-content">
            <div class="card-body">
                    <div class="d-flex justify-content-start align-items-center mb-1">
                        <div class="avatar mr-1">
                            <img src="{{ asset('images/profile/user-uploads/page-06.jpg') }}" alt="avtar img holder" height="45" width="45">
                        </div>
                        <div class="user-page-info">
                            <p id="idtextvendedor" class="mb-0">{{$campaña->vendedor->nombre}}</p>
                            <span id="idtextfecha" class="font-small-2">{{$campaña->created_at}}</span>
                        </div>
                        <div class="ml-auto user-like text-danger"><i class="fa fa-heart"></i></div>
                        </div>
                        <h6 id="idtexttitulo" class="mb-1">{{$campaña->titulo}}</h6>
                        <p id="idtextdescripcion">{{$campaña->descripcion}}</p>
                        <img class="img-fluid card-img-top rounded-sm" src="{{ asset('images/profile/post-media/3.jpg') }}" alt="avtar img holder">
                    </div>  
            </div>
            </div>
        </div>
    </div>
  </div>
</section>

@endsection
@section('vendor-script')
        <script src="{{ asset(mix('vendors/js/forms/validation/jqBootstrapValidation.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection
@section('page-script')
        <script src="{{ asset(mix('js/scripts/forms/validation/form-validation.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/pages/user-profile.js')) }}"></script>
@endsection
