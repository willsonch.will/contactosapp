@extends('layouts/contentLayoutMaster')
@section('title', 'App Calender')
@section('vendor-style')
@endsection
@section('page-style')
@endsection
@section('content')
<!-- Description -->
<div class="row" id="basic-table">
  <div class="col-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Listado de Vendedores</h4>
          </div>
          <div class="card-content">
              <div class="card-body">
                  <!-- Table with outer spacing -->
                  <div class="table-responsive">
                      <table class="table">
                      {{-- Filled Buttons start --}}
                      <a href="{{URL::action('VendedorController@create')}}">
                            <button id="addRow" class="btn btn-primary"><i class="feather icon-plus"></i>&nbsp; Registrar Nuevo Vendedor </button>
                        </a>
                        <br><br>
                      {{-- Filled Buttons end --}}
                          <thead>
                              <tr>
                                  <th>ID</th>
                                  <th>Nombre</th>
                                  <th>Celular</th>
                                  <th>Correo</th>
                                  <th>Fecha Nacimiento</th>
                                  <th>Sexo</th>
                              </tr>
                          </thead>
                          <tbody>
                          @foreach($vendedores as $v)
                              <tr>
                                  <th scope="row">{{$v->id}}</th>
                                  <td>{{$v->nombre}}</td>
                                  <td>{{$v->celular}}</td>
                                  <td>{{$v->correo}}</td>
                                  <td>{{$v->fecha_nacimiento}}</td>
                                  <td>{{$v->sexo}}</td>
                              </tr>
                            @endforeach
                          </tbody>
                      </table>
                  </div>
              </div>
              
          </div>
      </div>
  </div>
</div>

@endsection
@section('vendor-script')
@endsection
@section('page-script')
@endsection
