@extends('layouts/contentLayoutMaster')
@section('title', 'App Calender')
@section('vendor-style')
@endsection
@section('page-style')
@endsection
@section('content')
<!-- // Basic multiple Column Form section start -->
<section id="multiple-column-form">
  <div class="row match-height">
      <div class="col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Formulario Nuevo Vendedor</h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
                      <form class="form" method='POST' action='{{route("vendedor.create")}}'>
                      {{ csrf_field() }}
                          <div class="form-body">
                              <div class="row">
                                  <div class="col-md-6 col-12">
                                      <div class="form-label-group">
                                          <input type="text" id="first-name-column" class="form-control" placeholder="nombre completo" name="nombre" required>
                                          <label for="first-name-column">Nombre Completo</label>
                                      </div>
                                  </div>
                                  <div class="col-md-6 col-12">
                                      <div class="form-label-group">
                                          <input type="number" id="last-name-column" class="form-control" placeholder="celular" name="celular"  maxlength="8" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
                                          <label for="last-name-column">Celular</label>
                                      </div>
                                  </div>
                                  <div class="col-md-6 col-12">
                                      <div class="form-label-group">
                                          <input type="email" id="city-column" class="form-control" placeholder="correo electronico" name="correo" required>
                                          <label for="city-column">Correo Electronico</label>
                                      </div>
                                  </div>
                                  <div class="col-md-6 col-12">
                                      <div class="form-label-group">
                                          <input type="date" id="country-floating" class="form-control" name="fecha_nacimiento" placeholder="fecha de nacimiento">
                                          <label for="country-floating">Fecha de Nacimiento</label>
                                      </div>
                                  </div>
                                  <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <select class="form-control" id="" name="sexo" placeholder="sexo" required>
                                                <option value="O">Otro</option>
                                                <option value="F">Femenino</option>
                                                <option value="M">Masculino</option>
                                            </select>
                                                <label for="sexo">Sexo</label>
                                        </div>
                                  </div>
                                      
                                  </div>
                                  <div class="form-group col-12">
                  <fieldset class="checkbox">
                    <div class="vs-checkbox-con vs-checkbox-primary">
                      <input type="checkbox">
                      <span class="vs-checkbox">
                        <span class="vs-checkbox--check">
                          <i class="vs-icon feather icon-check"></i>
                        </span>
                      </span>
                      <span class="">Remember me</span>
                    </div>
                  </fieldset>
                </div>
                <div class="col-12">
                                      <button type="submit" class="btn btn-primary mr-1 mb-1">Registrar</button>
                                      <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Limpiar</button>
                                  </div>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>

@endsection
@section('vendor-script')
@endsection
@section('page-script')
@endsection
