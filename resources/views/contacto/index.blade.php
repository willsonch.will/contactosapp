@extends('layouts/contentLayoutMaster')
@section('title', 'App Calender')
@section('vendor-style')
@endsection
@section('page-style')
@endsection
@section('content')
<div class="row" id="basic-table">
  <div class="col-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Listado de Contactos</h4>
          </div>
          <div class="card-content">
              <div class="card-body">
                  <!-- Table with outer spacing -->
                  <div class="table-responsive">
                      <table class="table">
                      {{-- Filled Buttons start --}}
                      <a href="{{URL::action('ContactoController@create')}}">
                            <button id="addRow" class="btn btn-primary"><i class="feather icon-plus"></i>&nbsp; Registrar Nuevo Contacto </button>
                      </a>
                      <br><br>
                      {{-- Filled Buttons end --}}
                          <thead>
                              <tr>
                                  <th>ID</th>
                                  <th>Nombre</th>
                                  <th>Correo</th>
                                  <th>Celular</th>
                                  <th>Telefono</th>
                                  <th>Direccion</th>
                                  <th>Fecha Nacimiento</th>
                                  <th>Sexo</th>
                                  <th>Vendedor</th>
                                  <th>Intereses</th>
                              </tr>
                          </thead>
                          <tbody>
                          @foreach($contactos as $c)
                              <tr>
                                  <th scope="row">{{$c->id}}</th>
                                  <td>{{$c->nombre}}</td>
                                  <td>{{$c->correo}}</td>
                                  <td>{{$c->celular}}</td>
                                  <td>{{$c->telefono}}</td>
                                  <td>{{$c->direccion}}</td>
                                  <td>{{$c->fecha_nacimiento}}</td>
                                  <td>{{$c->sexo}}</td>
                                  <td>{{$c->vendedor->nombre}}</td>
                                  <td>
                                    @foreach($c->detalle_interes as $det_int)
                                        <p>{{$det_int->interes->descripcion}}</p>
                                    @endforeach
                                  </td>
                              </tr>
                            @endforeach
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>

@endsection
@section('vendor-script')
@endsection
@section('page-script')
@endsection
