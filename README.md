# Proyecto de la Materia COM450-Calidad del Software
## Aplicacion Contactos-Interes

La aplicacion consiste en registrar contactos, asociados a un determinado vendedor que tambien se registrara, los contactos se registran vinculados a determinados intereses, para luego poder generar camapañas que le sean de utilidad a los contactos

## Pre-requisitos 

* *PHP: 7.3v or Above*
* *Composer: 2.0.4v or Above*
* *Laravel: 8.0v or Above*
* *Laravel/UI: 3.0v or Above*
* *Laravel/Passport: 10.0v or Above*

## Instalacion

**Paso 1:** Clonar el repositorio

```
git clone https://gitlab.com/willsonch.will/contactosapp.git
cd contactosapp
```

**Paso 2:** Abra el terminal en el directorio raíz y para instalar los paquetes composer ejecute el siguiente comando:

```php
composer install	
```

**Paso 3:** En el directorio raíz, encontrará un archivo denominado .env , ejecute el siguiente comando para generar la clave (y también puede editar las credenciales de la base de datos aquí-Debe de crear una base de datos con las credenciales correspondientes). 

```php
php artisan key:generate	
```

**Paso 4:** Al ejecutar el siguiente comando, podrá obtener todas las dependencias de la carpeta node_modules:

```php
npm install
```

**Paso 5:** Para ejecutar el proyecto, debe ejecutar el siguiente comando en el directorio del proyecto. Se compilará los archivos php y todos los demás archivos de proyecto. Si está realizando cambios en cualquiera de los .php archivo, debe ejecutar el comando dado de nuevo.

```php
npm run dev
```
**Paso 6:** Para ejecutar las migraciones ejecutar el comando:

```php
php artisan migrate
```

**Paso 7:** Para servir la aplicación, debe ejecutar el siguiente comando en el directorio del proyecto. (Esto le dará una dirección con el número de puerto 8000.)

```php
php artisan serve
```

Ahora vaya a la dirección dada, verá que la aplicación se está ejecutando.

Para cambiar la dirección del puerto, ejecute el siguiente comando:

```php
php artisan serve --port=8080 // For port 8080
sudo php artisan serve --port=80 // If you want to run it on port 80, you probably need to sudo.
```

## Pruebas de Desarrollo

Para la ejecucion de los test se debe ejecutar en la terminal el comando:

```
vendor/bin/phpunit
```

## Tecnologias Usadas

* Laravel
* Vuexy

## Contribuidores del Proyecto

* Wilson Chojllu -Maintainer   [willsonch.will Gitlab](https://gitlab.com/willsonch.will)
* Sergio Cortez-Developer [SergioCortez10 Gitlab](https://gitlab.com/SergioCortez10)
* Ruddy Vladimir Plata Camargo-Developer [vladip11 Gitlab](https://gitlab.com/vladip11)


